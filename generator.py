import os
import random
from dataclasses import dataclass
from pathlib import Path

import minicli
from dotenv import load_dotenv
from jinja2 import Environment as Env
from jinja2 import FileSystemLoader

__version__ = "0.0.1"

load_dotenv()
BASE_URL = os.getenv(
    "BASE_URL", "https://umap-project.gitlab.io/leaflet-webcomponents/"
)
HERE = Path().resolve()
PUBLIC = HERE / "public"

environment = Env(loader=FileSystemLoader(str(HERE / "templates")))


@dataclass
class Marker:
    label: str
    lat: float
    lng: float

    @staticmethod
    def generate(count):
        markers = []
        for i in range(count):
            lat = random.randrange(5000, 5200)
            lng = random.randrange(0, 1000)
            marker = Marker(
                label=f"Marker {i+1}",
                lat=lat / 100,
                lng=lng / 100,
            )
            markers.append(marker)
        return markers


@minicli.cli
def build(count=1000):
    template_index = environment.get_template("index.html")
    content = template_index.render(markers=[], zoom=6, base_url=BASE_URL)
    Path(PUBLIC / "index.html").write_text(content)

    template_index = environment.get_template("index.html")
    content = template_index.render(
        markers=Marker.generate(count=count), zoom=6, base_url=BASE_URL
    )
    Path(PUBLIC / f"{count}.html").write_text(content)


if __name__ == "__main__":
    minicli.run()
