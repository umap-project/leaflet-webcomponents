from itertools import cycle
from pathlib import Path

import httpx
import minicli
import tomllib


def get_module_format(dependency):
    if "#" in dependency:
        return dependency.split("#", 1)
    else:
        return dependency, "esm"


def get_latest_version(dependency):
    if "==" in dependency:
        return dependency.split("==", 1)
    else:
        version = "latest"
    url = f"https://unpkg.com/browse/{dependency}@{version}/"
    print(f"Version from: {url}")
    response = httpx.head(url)
    if response.status_code >= 400:
        response.raise_for_status()
    return dependency, response.headers["Location"].split("@")[-1].strip("/")


def download_file(dependency, version, kind, module_format):
    dependency_name = dependency.split("/")[-1]
    root_url = f"https://unpkg.com/{dependency}@{version}"
    url_patterns = [
        f"/dist/{dependency_name}.{module_format}.{kind}",
        f"/dist/{dependency_name}-src.{module_format}.{kind}",
        f"/dist/{dependency_name}.{kind}",
        f"/dist/{dependency_name}.min.{kind}",
        f"/{kind}/{dependency_name}.{kind}",
    ]
    for url_pattern in url_patterns:
        url = f"{root_url}{url_pattern}"
        print(f"Trying: {url}")
        response = httpx.get(url, timeout=None)
        try:
            response.raise_for_status()
        except httpx.HTTPStatusError:
            continue
        break
    return response.text, url


def save_file(dependency, version, file_content, kind):
    target_path = Path() / "static" / "vendors"
    target_path.mkdir(parents=True, exist_ok=True)
    dependency_name = "-".join(part.strip("@") for part in dependency.split("/"))
    target_filename = target_path / f"{dependency_name}.{version}.{kind}"
    target_filename.write_text(file_content)
    return target_filename


@minicli.cli
def install():
    pyproject = tomllib.loads((Path() / "pyproject.toml").read_text())
    js_dependencies = pyproject["tool"]["npm"].get("js", [])
    js_kind_dependencies = list(zip(cycle(["js"]), js_dependencies))
    css_dependencies = pyproject["tool"]["npm"]["css"]
    css_kind_dependencies = list(zip(cycle(["css"]), css_dependencies))
    for kind, dependency in js_kind_dependencies + css_kind_dependencies:
        dependency, module_format = get_module_format(dependency)
        name, version = get_latest_version(dependency)
        file_content, url = download_file(name, version, kind, module_format)
        file_name = save_file(name, version, file_content, kind)
        print(f"{name}, {version}, saved in {file_name} from {url}")


if __name__ == "__main__":
    minicli.run()
