import * as L from './vendors/leaflet.1.9.4.js'

const WithEvents = (BaseClass) =>
  class extends BaseClass {
    emit(type, detail = {}, emitterName = undefined) {
      const event = new CustomEvent(`${emitterName || this.localName}:${type}`, {
        bubbles: true,
        cancelable: true,
        detail: detail,
      })
      return this.dispatchEvent(event)
    }
  }

class LeafletMap extends WithEvents(HTMLElement) {
  constructor() {
    super()
    this.attachShadow({ mode: 'open' })
  }

  #attachCSS(path) {
    const linkElement = document.createElement('link')
    linkElement.setAttribute('rel', 'stylesheet')
    linkElement.setAttribute('href', path)
    this.shadowRoot.appendChild(linkElement)
  }

  #addCSS(css) {
    const linkElement = document.createElement('style')
    linkElement.textContent = css
    this.shadowRoot.appendChild(linkElement)
  }

  #createMapContainer() {
    const mapContainer = document.createElement('div')
    mapContainer.style.height = this.dataset.height
    mapContainer.style.width = this.dataset.width
    mapContainer.style.margin = '0 auto'
    this.shadowRoot.appendChild(mapContainer)
    return mapContainer
  }

  #createMap(mapContainer) {
    const map = L.map(mapContainer).setView(
      [
        this.querySelector('[data-latitude]').textContent,
        this.querySelector('[data-longitude]').textContent,
      ],
      this.querySelector('[data-zoom]').textContent
    )
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map)
    return map
  }

  connectedCallback() {
    this.#attachCSS('static/vendors/leaflet.css')
    this.#attachCSS('static/vendors/picocss-pico.conditional.2.0.6.css')
    this.#addCSS(`
      [aria-current="location"] th,
      [aria-current="location"] td {
        background-color: var(--pico-mark-background-color) !important;
      }
    `)
    const mapContainer = this.#createMapContainer()
    const map = this.#createMap(mapContainer)
    const table = this.querySelector('leaflet-table, [is="leaflet-table"]')
    if (table) {
      // The figure element will serve as a scrolling container of the table.
      const figure = document.createElement('figure')
      figure.classList.add('pico')
      figure.style.margin = '0'
      figure.style.padding = '0'
      figure.style.display = 'block'
      figure.style.maxHeight = '300px'
      figure.style.overflowY = 'scroll'
      figure.appendChild(table)
      this.shadowRoot.appendChild(figure)
    }

    // Create markers on leaflet-marker's components `new` events.
    this.addEventListener('leaflet-marker:new', (event) => {
      const marker = L.marker([event.detail.latitude, event.detail.longitude])
        .addTo(map)
        .bindPopup(event.detail.label)
      marker._icon.setAttribute('id', `marker_${marker._leaflet_id}`)
      this.emit('marker-added', { marker, map })
    })
  }
}

class LeafletTable extends HTMLElement {
  #setupTable() {
    this.style.width = '100%'
    this.style.textAlign = 'center'
    this.style.margin = '1rem 0'
    const thead = document.createElement('thead')
    thead.innerHTML = `
      <tr>
        <th scope="col">Label</th>
        <th scope="col">Latitude</th>
        <th scope="col">Longitude</th>
      </tr>
    `
    this.appendChild(thead)
    const tbody = document.createElement('tbody')
    this.appendChild(tbody)
    return tbody
  }

  #clearCurrentSelection() {
    for (const tr of this.querySelectorAll('tr')) {
      tr.removeAttribute('aria-current')
    }
  }

  #addRow(container, { marker, map }) {
    const markerId = marker._icon.getAttribute('id')
    const rowId = `details_${markerId}`
    const row = document.createElement('tr')
    row.setAttribute('id', rowId)
    marker._icon.setAttribute('aria-details', rowId)
    const label = document.createElement('th')
    label.setAttribute('scope', 'row')
    const link = document.createElement('a')
    link.href = `#${markerId}`
    link.textContent = marker._popup._content

    link.addEventListener('click', (e) => {
      e.preventDefault()
      marker.openPopup()
      const prefersReducedMotion = window.matchMedia(
        '(prefers-reduced-motion: reduce)'
      ).matches
      if (prefersReducedMotion !== true) {
        map.flyTo(marker.getLatLng(), 12)
      }
      this.#clearCurrentSelection()
      row.setAttribute('aria-current', 'location')
    })
    map.addEventListener('popupopen', (event) => {
      const currentRow = this.querySelector(
        `tr#details_${event.popup._source._icon.id}`
      )
      currentRow?.setAttribute('aria-current', 'location')
    })
    map.addEventListener('popupclose', (event) => {
      this.#clearCurrentSelection()
    })

    label.appendChild(link)
    row.appendChild(label)

    const lat = document.createElement('td')
    lat.textContent = marker._latlng.lat
    row.appendChild(lat)

    const lng = document.createElement('td')
    lng.textContent = marker._latlng.lng
    row.appendChild(lng)

    container.appendChild(row)
  }

  connectedCallback() {
    const tbody = this.#setupTable()

    // Create new row on leaflet-marker's components `new` events.
    this.getRootNode().host.addEventListener('leaflet-map:marker-added', (event) => {
      this.#addRow(tbody, event.detail)
    })
  }
}

class LeafletMarker extends WithEvents(HTMLElement) {
  connectedCallback() {
    this.emit(
      'new',
      {
        latitude: this.querySelector('[data-latitude]').textContent,
        longitude: this.querySelector('[data-longitude]').textContent,
        label: this.querySelector('[data-label]').textContent,
      },
      'leaflet-marker' // Required because we use `is=`.
    )
  }
}

function register(Class, tagName) {
  if ('customElements' in window && !customElements.get(tagName)) {
    customElements.define(tagName, Class)
  }
}

export { LeafletMap, LeafletTable, LeafletMarker, register }
