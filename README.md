# Leaflet WebComponents

Experimentations around Web Components and Leaflet maps,
toward a more accessible content.

## Install

Pre-requisite: Python3

Create and activate the virtualenv:

    $ python3 -m venv venv
    $ source venv/bin/activate

Install dependencies:

    $ make install

## Environment

Create a `.env` file at the root of this directory and put `BASE_URL="/"` in it.


## Building

Launch command:

    $ make build

## Running

Run local server:

    $ make serve

Go to http://127.0.0.1:8000/
